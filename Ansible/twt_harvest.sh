#!/usr/bin/env bash

array=$(cat IP.txt)
export mainnode=`echo ${array} | cut -f1 -d' '`
git clone https://gitlab.eng.unimelb.edu.au/framosmorale/comp90024.git
apt install python3-pip -y
pip3 install -r ./comp90024/Data\ collection/requirements.txt
python3 ./comp90024/Data\ collection/streamFile_to_couch.py "$mainnode"
python3 ./comp90024/Data\ collection/searchFile_to_couch.py.py "$mainnode"
python3 ./comp90024/Data\ collection/tweet_gatherer_app.py stream 0 "$mainnode" & python3 ./comp90024/Data\ collection/tweet_gatherer_app.py search 1 "$mainnode" & python3 ./comp90024/Data\ collection/tweet_gatherer_app.py stream 2 "$mainnode" & python3 ./comp90024/Data\ collection/tweet_gatherer_app.py stream 3 "$mainnode" & python3 ./comp90024/Data\ collection/tweet_gatherer_app.py stream 4 "$mainnode" & 
