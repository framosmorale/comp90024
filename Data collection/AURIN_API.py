import configparser
import urllib.request
from lxml import etree

username='student'
password='dj78dfGF'

def openapi_request(url):

    # create an authenticated HTTP handler and submit URL
    password_manager = urllib.request.HTTPPasswordMgrWithDefaultRealm()
    password_manager.add_password(None, url, username, password)
    auth_manager = urllib.request.HTTPBasicAuthHandler(password_manager)
    opener = urllib.request.build_opener(auth_manager)
    urllib.request.install_opener(opener)

    req = urllib.request.Request(url)
    handler = urllib.request.urlopen(req)

    return handler.read()

dataset = 'aurin:datasource-AU_Govt_ABS_Census-UoM_AURIN_DB_2_gccsa_p11b_english_profic_by_arrival_yr_by_sex_census_2016'
url ='http://openapi.aurin.org.au/wfs?request=DescribeFeatureType&service=WFS&version=1.1.0&typeName='+dataset
xml = openapi_request(url)
root = etree.fromstring(xml)
import xmltodict, json
out = xmltodict.parse(xml)