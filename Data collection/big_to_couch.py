import json
import couchdb
import sys
from preprocessVM import save2Couch, updateCDB, replaceCDB, classifyTwt

couchIP = sys.argv[1]
#"172.26.130.79"
storeCouch = True

couchserver = couchdb.Server("http://admin:admin@"+couchIP+":5984/")
db = couchserver['live_demo2']
coords = []
areas = {}

with open('./comp90024/Data collection/sets.json') as f:
    for line in f:
        c = json.loads(line)
        areas[c['region']] = c['box']


        
def insideArea(x,y,poly):
    ###
    # poly dimension [xmin, xmax, ymin, ymax]
    ###
    xmin, ymin, xmax, ymax = poly
    inside = False
    if x > xmin and x <= xmax:
        if y > ymin and y <= ymax:
            inside = True
    return inside


def checkArea(point):
    check = 'None'
    for k, v in areas.items():
        if insideArea(point[0],point[1],v):
            check = k
    return check   

filtered = {}
with open('big.json', 'r', encoding = 'utf-8') as file:
    first = file.readline()
    for line in file:
        twt = None
        if line[-2] == ',':
            twt = json.loads(line[:-2])
        elif line[-3]==']':
            #big.append(json.loads(line[:-3]))
            pass
        else:
            twt = json.loads(line[:-1])

        if twt is not None:
            coor = twt['value']['geometry']['coordinates']
            tweet = twt['doc']
            tweet['tags'] = classifyTwt(tweet)
            del tweet['_rev']
            
            if len(tweet['tags']) > 0:

                block = checkArea(coor)
                if block == 'None':
                    if 'location' in tweet['user']:
                        block = tweet['user']['location']
                        block = block.split(',')[0]
                tweet['region'] = block
                #save2Couch(tweet, db)
                filtered[tweet['id_str']]=tweet


with open("big_filtered.json", "w") as outfile: 
    json.dump(filtered, outfile)


                
