import json
import couchdb
import sys
from preprocessVM import save2Couch, updateCDB, replaceCDB, classifyTwt

couchIP = sys.argv[1]
#"172.26.130.79"
storeCouch = True

couchserver = couchdb.Server("http://admin:admin@"+couchIP+":5984/")
db = couchserver['live_demo2']


with open('./comp90024/Data collection//'+'big_filtered.json', 'r', encoding = 'utf-8') as file:
    biga = json.load(file)

    
for k,v in biga.items():
    loc = v['location']
    v['region'] = loc.capitalize() 
    v['tags'] = list(set(v['tags']))
    save2Couch(v, db)