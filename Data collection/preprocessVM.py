
def save2Couch(twt, db, w_replace = False):
    doc = twt
    doc['_id'] = doc['id_str']
    try:
        db.save(doc)
    except:
        if w_replace:
            replaceCDB(doc, db)


def updateCDB(twt, db):
    doc = db.get(twt['id_str'])
    up = twt['tags']
    doc['tags'] = up
    db[twt['_id']] = doc
    print('update:')


def replaceCDB(twt,db):
    del db[twt['id_str']]
    db.save(twt)


def classifyTwt(twt):
    tags = []
    sports = ['AFL', 'tennis', 'footie','swimming','AustralianOpen', 'footy' ,'soccer', 'cricket', '#AFL', 'netball', 'basketball', 'NRL', '#NRL', 'rugby']

    if 'text' in twt:
        text =  twt['text']
    else:
        text = twt['full_text']
    for s in sports:
        s = s.lower()
        if s in text.lower():
            if s in ['afl','footie','#afl','footy', '#afl']:
                tag = 'Footie'
            elif s in ['tennis', 'australianopen']:
                tag = 'Tennis'
            elif s in ['nrl', '#nrl', 'rugby']:
                tag = 'Rugby'
            else:
                tag = s
            tags.append(tag)
    tags = list(set(tags))
    return tags