
import json
import couchdb
import os
import sys
from preprocessVM import save2Couch, updateCDB, replaceCDB, classifyTwt

couchIP = sys.argv[1]
#"172.26.130.79"
storeCouch = True

couchserver = couchdb.Server("http://admin:admin@"+couchIP+":5984/")
db = couchserver['live_demo2']
data = {}


for filename in os.listdir('./comp90024/Data collection/UPLOAD/'):
    if filename.startswith('twt_search'):
        with open('./comp90024/Data collection/UPLOAD/'+filename, 'r', encoding = 'utf-8') as file:
            for line in file:
                #line = ast.literal_eval(line)
                line = json.loads(line)
                for k,v in line.items():
                    data[k] = v

for k,v in data.items():
    v['tags'] = classifyTwt(v)
    save2Couch(v,db)