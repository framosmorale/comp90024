import json
import codecs
import ast
import couchdb
import os
import sys
from preprocessVM import save2Couch, updateCDB, replaceCDB, classifyTwt

couchIP = sys.argv[1]
#"172.26.130.79"
storeCouch = True

couchserver = couchdb.Server("http://admin:admin@"+couchIP+":5984/")
db = couchserver.create("live_demo2")
#db = couchserver['live_demo']

data = []
log = []


for filename in os.listdir('./comp90024/Data collection/UPLOAD/'):
    if filename.startswith('twt_stream'):
        with open('./comp90024/Data collection/UPLOAD/'+filename, 'r', encoding = 'utf-8') as file:
            for line in file:
                try:
                    line = ast.literal_eval(line)
                    data.append(line)
                except:
                    log.append(filename)

for i in data:
    twt = next(iter(i.values()))
    twt['tags'] = classifyTwt(twt)
    if len(twt['tags'])> 0:
        save2Couch(twt, db)