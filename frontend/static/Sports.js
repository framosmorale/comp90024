var sports = [];
var counts = [];

var sports_by_city = [];
var counts_by_city = [];
var baseUrl = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');


var response_value = function(){
    $.ajax({
        async: false,
        url: baseUrl+'/couchdb/live_demo2/_design/sports_count/_view/sports_count?group=true&group_level=1',
        type: 'get',
        dataType: 'json',
        headers: {
            'Authorization': 'Basic ' + btoa('admin:admin'),
        },
        xhrFields: {
            withCredentials: true
        },
        success: function(data) {
            temp = data;
            console.log(data);
        },
        error: function() {
            alert('Error occurs!');
        },
        reduce: false
    });
    console.log(temp);
    return temp;
}();
data_rows = response_value["rows"];
for (row in data_rows) {
    sports.push(data_rows[row]["key"][0]);
    counts.push(data_rows[row]["value"]);
};

var city;
function setByCity(){
  city = document.getElementById('city').value;
  console.log(city);

  var response_by_city = function(){
    $.ajax({
        async: false,
        crossDomain: true,
        url: baseUrl+'/couchdb/live_demo2/_design/sports_count/_view/sports_count?group=true&group_level=2',
        type: 'get',
        dataType: 'json',
        headers: {
            'Authorization': 'Basic ' + btoa('admin:admin'),
            // 'Access-Control-Allow-Origin': 'localhost',
            // 'Access-Control-Allow-Credentials' : true,
        },
        xhrFields: {
            withCredentials: true
        },
        success: function(data) {
            temp = data;
            console.log(data);
        },
        error: function() {
            alert('Error occurs!');
        },
        reduce: false
    });
    console.log(temp);
    return temp;
}();
console.log("RESPONSE:"+response_by_city);
for(var i =0;i<sports_by_city.length;i++)
{
    removeData(myChart1);
}
sports_by_city =[]
counts_by_city = []



data_rows = response_by_city["rows"];
for (row in data_rows) {
    if(data_rows[row]["key"][1] == city)
    {
        console.log(data_rows[row]["key"][1]);
        sports_by_city.push(data_rows[row]["key"][0]);
        counts_by_city.push(data_rows[row]["value"]);
        addData(myChart1,data_rows[row]["key"][0],data_rows[row]["value"]);
    }
}

myChart1.update();
}

var sport;
var city_by_sport = [];
var counts_by_sport = [];
function sendSport(){
  sport = document.getElementById('sport').value;
  console.log(sport);

  var response_by_city = function(){
    $.ajax({
        async: false,
        crossDomain: true,
        url: baseUrl+'/couchdb/live_demo2/_design/retweets_followers/_view/retweets_followers?group=true',
        type: 'get',
        dataType: 'json',
        headers: {
            'Authorization': 'Basic ' + btoa('admin:admin'),
            // 'Access-Control-Allow-Origin': 'localhost',
            // 'Access-Control-Allow-Credentials' : true,
        },
        xhrFields: {
            withCredentials: true
        },
        success: function(data) {
            temp = data;
            console.log(data);
        },
        error: function() {
            alert('Error occurs!');
        },
        reduce: false
    });
    console.log(temp);
    return temp;
}();
console.log("RESPONSE:"+response_by_city);
for(var i =0;i<city_by_sport.length;i++)
{
    removeData(myChart2);
}

city_by_sport =[]
counts_by_city = []

data_rows = response_by_city["rows"];
for (row in data_rows) {
    console.log(sport);
    console.log(data_rows[row]["key"][1]);
    if(data_rows[row]["key"][1] == sport)
    {
        city_by_sport.push(data_rows[row]["key"][0]);
        console.log(data_rows[row]["value"][0]["sum"]);
        console.log(data_rows[row]["value"][0]["count"]);
        var avg = data_rows[row]["value"][0]["sum"] / data_rows[row]["value"][0]["count"] ;
        counts_by_city.push(avg);
        addData(myChart2,data_rows[row]["key"][0],avg);
    }
}

myChart2.update();
}

var nodes = new vis.DataSet();
var edges =  new vis.DataSet();
var ids =[];
var nodesexist =[];
function networkGraph()
{
    city = document.getElementById('citynetwork').value;
    sport = document.getElementById('sportnetwork').value;

    var response_network = function(){
        $.ajax({
            async: false,
            crossDomain: true,
            url: baseUrl+'/couchdb/live_demo/_design/replies/_view/replies?group=true',
            type: 'get',
            dataType: 'json',
            headers: {
                'Authorization': 'Basic ' + btoa('admin:admin'),
                // 'Access-Control-Allow-Origin': '*',
                // 'Access-Control-Allow-Credentials' : true,
            },
            xhrFields: {
                withCredentials: true
            },
            success: function(data) {
                temp = data;
                console.log(data);
            },
            error: function() {
                alert('Error occurs!');
            },
            reduce: false
        });
        console.log(temp);
        return temp;
    }();
    data_rows = response_network["rows"];
    var i=1;
    ids =[];
    nodesexist = [];
    nodes = new vis.DataSet();
    edges =  new vis.DataSet();
    for (row in data_rows) {
        if(data_rows[row]["key"][0] == city && data_rows[row]["key"][1] == sport )
        {
            if(!(data_rows[row]["key"][2] in nodesexist))
            {
                nodes.add({id: i,label:data_rows[row]["key"][2]});
                nodesexist.push(data_rows[row]["key"][2]);
                ids.push(i);
                var id1=i;
                i++;
            }
            else
            {
                id1  = nodesexist.indexOf(data_rows[row]["key"][2])
            }
            if(!(data_rows[row]["key"][3] in nodesexist))
            {
                nodes.add({id: i,label:data_rows[row]["key"][3]});
                nodesexist.push(data_rows[row]["key"][3]);
                ids.push(i);
                var id2=i;
                i++;
            }
            else
            {
                id2  = nodesexist.indexOf(data_rows[row]["key"][3])
            }

            edges.add({from:id1,to:id2});

        }
    };
    var container = document.getElementById("mynetwork");
    var data = {
        nodes: nodes,
        edges: edges,
      };
    var options = {};
    var network = new vis.Network(container, data, options);

}

var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: sports,
        datasets: [{
            label: '# of Tweets',
            data: counts,
            backgroundColor: [
                'rgba(255, 99, 132, 0.5)',
                'rgba(54, 162, 235, 0.5)',
                'rgba(255, 206, 86, 0.5)',
                'rgba(75, 192, 192, 0.5)',
                'rgba(153, 102, 255, 0.5)',
                'rgba(255, 159, 64, 0.5)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            },
            // yAxes: [{
            //     ticks: {
            //         fontSize: 40
            //     }
            // }]
        }
    }
});

var ctx = document.getElementById('myChart1').getContext('2d');
var myChart1 = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: sports_by_city ,
        datasets: [{
            label: '# of Tweets',
            data: counts_by_city,
            backgroundColor: [
                'rgba(255, 99, 132, 0.5)',
                'rgba(54, 162, 235, 0.5)',
                'rgba(255, 206, 86, 0.5)',
                'rgba(75, 192, 192, 0.5)',
                'rgba(153, 102, 255, 0.5)',
                'rgba(255, 159, 64, 0.5)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            },
            // yAxes: [{
            //     ticks: {
            //         fontSize: 40
            //     }
            // }]
        }
    }
});

var ctx = document.getElementById('myChart2').getContext('2d');
var myChart2 = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: city_by_sport,
        datasets: [{
            label: '# Average of Retweets',
            data: counts_by_sport,
            backgroundColor: [
                'rgba(255, 99, 132, 0.5)',
                'rgba(54, 162, 235, 0.5)',
                'rgba(255, 206, 86, 0.5)',
                'rgba(75, 192, 192, 0.5)',
                'rgba(153, 102, 255, 0.5)',
                'rgba(255, 159, 64, 0.5)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});




function removeData(chart) {
    chart.data.labels.pop();
    chart.data.datasets.forEach((dataset) => {
        dataset.data.pop();
    });
    chart.update();
}

function addData(chart, label, data) {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });
    chart.update();
}
